import numpy as np
import trimesh
import argparse
import os
import os.path as osp
import logging
import re


def process_mesh(dataset_name, object_name, object_mesh_folder): 
    if dataset_name == 'BigBird':
        object_mesh_path = object_mesh_folder + '/' + object_name + '/' + \
                'meshes/poisson.ply' 
        proc_mesh_path = object_mesh_folder + '/' + object_name + '/' + \
                'meshes/poisson_proc.stl' 
    elif dataset_name == 'GraspDatabase' or dataset_name == 'YCB':
        object_mesh_path = object_mesh_folder + '/' + object_name + \
                            '/' + object_name + '.stl' 
        proc_mesh_path = object_mesh_folder + '/' + object_name + \
                            '/' + object_name + '_proc.stl' 
    mesh = trimesh.load(object_mesh_path)
    # print 'Vertices #: ', mesh.vertices.shape[0]
    mesh.fix_normals()
    # if mesh.volume < 0.:
    #     # print mesh.volume
    #     mesh.invert()
    # print 'Vertices # after proc:', mesh.vertices.shape[0]
    _ = mesh.export(proc_mesh_path)


def get_object_mass_and_inertia(object_mesh_path):
    '''
        Get the mass and inertia of given object mesh.
    '''
    # Set the density of the object mesh, the default density 
    # of trimesh is 1
    # Assume the density is 100kg/m^3, tenth of the water density
    mesh = trimesh.load(object_mesh_path)
    mesh.density = 100.
    return mesh.mass, mesh.moment_inertia


def generate_urdf(object_name, object_mesh_paths, object_pose = [0.0] * 6, gazebo = False):
    '''
        Generate the urdf for a given object mesh with the pose, mass and inertia as
        parameters.
    '''
    object_mass, object_inertia = get_object_mass_and_inertia(object_mesh_paths[0])
    object_rpy = str(object_pose[0]) + ' ' + str(object_pose[1]) + ' ' + str(object_pose[2])
    object_location = str(object_pose[3]) + ' ' + str(object_pose[4]) + ' ' + str(object_pose[5])
    urdf_str = """
    <robot name=\"""" + object_name + """\">
    <link name=\"""" + object_name + """_link">
    <inertial>
      <!-- Note the inertia values and COM are not accurate, however they get computed in
       Isaac Gym so as long as you're only using it there it's irrelevant -->
      <origin xyz=\"""" + str(object_location) +"""\"  rpy=\"""" + str(object_rpy) +"""\"/>
      <mass value=\"""" + str(object_mass) + """\" />
      <inertia  ixx=\"""" + str(object_inertia[0][0]) + """\" ixy=\"""" + str(object_inertia[0][1]) + """\"  ixz=\"""" + \
              str(object_inertia[0][2]) + """\"  iyy=\"""" + str(object_inertia[1][1]) + """\"  iyz=\"""" + str(object_inertia[1][2]) + \
              """\"  izz=\"""" + str(object_inertia[2][2]) + """\" />
    </inertial>
    
    <visual>
      <origin xyz=\"""" + str(object_location) +"""\"  rpy=\"""" + str(object_rpy) +"""\"/>
      <geometry>
        <mesh filename=\"""" + object_mesh_paths[1] + """\" />
      </geometry>
    </visual>

    <collision>
      <origin xyz=\"""" + str(object_location) +"""\"  rpy=\"""" + str(object_rpy) +"""\"/>
      <geometry>
        <mesh filename=\"""" + object_mesh_paths[0] + """\" />
      </geometry>
    </collision>
    </link>
    """
    if gazebo:
        urdf_str += """
        <gazebo reference=\"""" + object_name + """_link\">
           <mu1>1.0</mu1>
           <mu2>1.0</mu2>
           <material>Gazebo/Red</material>
        </gazebo>
        """
    urdf_str += """
    </robot>
    """
    return urdf_str

def gen_mtl():
    mtl_string = """# Blender MTL File: 'None'
    # Material Count: 1

    newmtl material_0
    Ns 0.000000
    Ka 0.200000 0.200000 0.200000
    Kd 1.000000 1.000000 1.000000
    Ks 1.000000 1.000000 1.000000
    Ke 0.000000 0.000000 0.000000
    Ni 1.000000
    d 1.000000
    illum 2
    map_Kd texture_map.png"""
    return mtl_string


def get_dataset_dir(repo_name, data_root):
    path = osp.join(data_root, repo_name)
    if osp.exists(path):
        return path
    return None

def get_object_names_and_paths(repo_name, data_root):
    paths = {}
    data_dir = get_dataset_dir(repo_name, data_root)
    dirs = os.listdir(data_dir)
    if repo_name == 'ycb-fixed-meshes':
        regex  = r'^[0-9]*_|^[0-9]*-'
        for o in dirs:
            coll_mesh_path = osp.join(data_dir, osp.join(o, 'google_16k/nontextured_proc.stl'))
            textured_mesh_path = osp.join(data_dir, osp.join(o, 'google_16k/textured.obj'))
            mtl_mesh_path = osp.join(data_dir, osp.join(o, 'google_16k/textured.mtl'))
            if osp.exists(coll_mesh_path):
                if osp.exists(textured_mesh_path):
                    paths[re.sub(regex, '', o)] = [coll_mesh_path, textured_mesh_path, mtl_mesh_path]
                else:
                    paths[re.sub(regex, '', o)] = [coll_mesh_path, coll_mesh_path, mtl_mesh_path]
    return paths

def rewrite_mtl(path):
    if osp.exists(path) and not osp.exists(path+".bk"):
        print(f"Backing up {path} to {path}.bk")
        os.rename(path, path+".bk")
    with open(path, 'w') as f:
        print(f"Writing {path}")
        f.write(gen_mtl())
        

if __name__=='__main__':
    DATA_ROOT = os.environ.get("DATA_ROOT")
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--repo_name", type=str, default='ycb-fixed-meshes')
    parser.add_argument("-d", "--data_root", type=str, default=DATA_ROOT)
    parser.add_argument("-s", "--urdf_save_path", type=str, default=None)
    parser.add_argument("-m", "--rewrite_mtl", action='store_true')
    args = parser.parse_args()
    
    if args.data_root is None:
        logging.error("Specify data root with -d / --data_root or "
                      "set the DATA_ROOT environment variable")
        exit(1)
    if not osp.exists(args.data_root):
        logging.error(f"Data root specified does not exist: {args.data_root}")
        exit(1)

    data_path = get_dataset_dir(args.repo_name, args.data_root)

    if data_path is None:
        logging.error(f"Data path: {data_path} doesn't exist, check if repo is downloaded")
    
    obj_paths = get_object_names_and_paths(args.repo_name, args.data_root)
    
    for object_name, mesh_paths in obj_paths.items():
        #process_mesh(dataset_name, object_name, object_mesh_folder)        
        urdf = generate_urdf(object_name, mesh_paths)
        save_path = args.urdf_save_path
        if save_path is None:
            save_path = osp.dirname(mesh_paths[0])
        urdf_path = osp.join(save_path, object_name+".urdf")
        print(f'writing {object_name} to {urdf_path}')
        if args.rewrite_mtl:
            rewrite_mtl(mesh_paths[2])
            
        with open(urdf_path, 'w') as f:
            f.write(urdf)
