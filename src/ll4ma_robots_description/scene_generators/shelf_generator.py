#!/usr/bin/env python
import os.path as osp
import argparse
import trimesh
import numpy as np
from lxml import etree

from ll4ma_util import file_util


class ShelfGenerator:

    @classmethod
    def create_mesh(
            self,
            width,
            height,
            depth,
            thickness=0.02,
            n_shelves=0,
            shelf_heights=[],
            exclude_bottom_wall=False,
            return_component_meshes=False
    ):
        W, H, D, T = width, height, depth, thickness
        back_wall = trimesh.creation.box(
            extents=(T, W, H),
            transform=np.array([
                [1, 0, 0, -D/2. + T/2.],
                [0, 1, 0, 0],
                [0, 0, 1, 0.],
                [0, 0, 0, 1]
            ])
        )
        right_wall = trimesh.creation.box(
            extents=(D, T, H),
            transform=np.array([
                [1, 0, 0, 0],
                [0, 1, 0, W/2. - T/2.],
                [0, 0, 1, 0.],
                [0, 0, 0, 1]
            ])
        )
        left_wall = trimesh.creation.box(
            extents=(D, T, H),
            transform=np.array([
                [1, 0, 0, 0],
                [0, 1, 0, -W/2. + T/2.],
                [0, 0, 1, 0.],
                [0, 0, 0, 1]
            ])
        )
        top_wall = trimesh.creation.box(
            extents=(D, W, T),
            transform=np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, H/2. - T/2.],
                [0, 0, 0, 1]
            ])
        )
        bottom_wall = trimesh.creation.box(
            extents=(D, W, T),
            transform=np.array([
                [1, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 0, 1, -H/2. + T/2.],
                [0, 0, 0, 1]
            ])
        )
        
        walls = [
            back_wall,
            right_wall,
            left_wall,
            top_wall
        ]
        if not exclude_bottom_wall:
            walls.append(bottom_wall)
        
        shelves = []
        if shelf_heights:
            for shelf_height in shelf_heights:
                shelf = trimesh.creation.box(
                    extents=(D, W, T),
                    transform=np.array([
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 1, -H/2. -T/2. + shelf_height],
                        [0, 0, 0, 1]
                    ])
                )
                shelves.append(shelf)
        elif n_shelves > 0:
            spacing = H / (n_shelves + 1)
            shelf_height = spacing
            for _ in range(n_shelves):
                shelf = trimesh.creation.box(
                    extents=(D, W, T),
                    transform=np.array([
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 1, -H/2. -T/2. + shelf_height],
                        [0, 0, 0, 1]
                    ])
                )
                shelves.append(shelf)
                shelf_height += spacing

        components = walls + shelves
        shelf = trimesh.util.concatenate(components)
        return (shelf, components) if return_component_meshes else shelf
            

    @classmethod
    def create_urdf(
            self,
            save_dir,
            name,
            mass=10.
    ):
        mesh_fn = f"{name}.stl"
        robot = etree.Element("robot", name=name)
        link = etree.SubElement(robot, "link", name=name)
        visual = etree.SubElement(link, "visual")
        self._add_mesh_block(visual, mesh_fn)
        collision = etree.SubElement(link, "collision")
        self._add_mesh_block(collision, mesh_fn)
        inertial = etree.SubElement(link, "inertial")
        mass = etree.SubElement(inertial, "mass", value=str(mass))
        # Inertia probably doesn't matter much since shelf will likely be a fixture of env
        inertia = etree.SubElement(
            inertial,
            "inertia",
            ixx="1.0",
            ixy="0.0",
            ixz="0.0",
            iyy="1.0",
            iyz="0.0",
            izz="1.0"
        )   
        urdf = etree.ElementTree(robot)
        return urdf

    @classmethod
    def _add_mesh_block(self, element, mesh_fn):
        etree.SubElement(element, "origin", xyz="0.0 0.0 0.0")
        geometry = etree.SubElement(element, "geometry")
        etree.SubElement(geometry, "mesh", filename=mesh_fn)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # MESH OPTIONS ====================================================================
    parser.add_argument(
        '--width', type=float, default=0.8,
        help="Width of shelf in meters (outer left edge to outer right edge)"
    )
    parser.add_argument(
        '--height', type=float, default=1.5,
        help="Height of shelf in meters (floor to upper edge)"
    )
    parser.add_argument(
        '--depth', type=float, default=0.5,
        help="Depth of shelf in meters (outer front edge to outer back edge)"
    )
    parser.add_argument(
        '--n_shelves', type=int, default=2,
        help=("Number of evenly spaced shelves to include on interior. This gets "
              "overridden by --shelf_heights if that's specified.")
    )
    parser.add_argument(
        '--shelf_heights', type=float, nargs='+', default=[],
        help=("List of heights for shelves in meters (height from floor to top of "
              "shelf surface). This option overrides --n_shelves.")
    )
    parser.add_argument(
        '--wall_thickness', type=float, default=0.02,
        help="Thickness of walls and shelves in meters"
    )
    parser.add_argument(
        '--exclude_bottom_wall', action='store_true',
        help="Exclude the bottom wall if flag set"
    )
    parser.add_argument(
        '--show', action='store_true',
        help="Visualize mesh"
    )
    parser.add_argument(
        '--no_show', dest='show', action='store_false',
        help="Do not visualize mesh"
    )
    parser.set_defaults(show=True)
    # FILE SAVING OPTIONS =============================================================
    parser.add_argument(
        '--save_dir', type=str, default=None,
        help="Absolute path to directory to save mesh and URDF"
    )
    parser.add_argument(
        '--name', type=str, default='shelf',
        help=("Determines file prefix for saving and link name in URDF. "
              "Will be {save_dir}/{prefix}.stl for mesh and "
              "{save_dir}/{prefix}.urdf for URDF")
    )
    # URDF OPTIONS ====================================================================
    parser.add_argument(
        '--mass', type=float, default=10.,
        help="Mass in kg of the entire shelf for URDF"
    )
    args = parser.parse_args()


    generator = ShelfGenerator()
    
    mesh = generator.create_mesh(
        args.width,
        args.height,
        args.depth,
        args.wall_thickness,
        args.n_shelves,
        args.shelf_heights,
        args.exclude_bottom_wall,
    )
    if args.show:
        scene = mesh.scene()
        scene.set_camera((np.pi/2., 0., np.pi/2.))
        scene.show()

    if args.save_dir:
        file_util.create_dir(args.save_dir)
        mesh_fn = osp.join(args.save_dir, f"{args.name}.stl")
        urdf_fn = osp.join(args.save_dir, f"{args.name}.urdf")

        urdf = generator.create_urdf(
            args.save_dir,
            args.name,
            args.mass
        )
        
        mesh.export(mesh_fn)
        urdf.write(urdf_fn, xml_declaration=True, encoding="utf-8", pretty_print=True)
        
